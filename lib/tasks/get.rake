namespace :get do
  # rake get:recipe
  desc "Get todays recipe and email it"
  task recipe: :environment do
    theMeal = Nokogiri::HTML(HTTParty.get('http://vadfanskajaglagatillmiddag.nu/'))
    baseMeal = theMeal.xpath('//*[@id="fredag"]/h1[2]/font/a')
    plateName = baseMeal.text
    recipeLink = baseMeal.attr('href')
    
    mg_client = Mailgun::Client.new ENV['MAILGUN_SECRET']

    emails = ['kim@pistoleros.se', 'hello@dainelvera.com']
    
    puts plateName
    puts recipeLink
    puts "*"*100

    emails.each do |email|
      puts email

      message_params = { 
        from: 'postmaster@whatshouldimakefordinner.today',
        to: email,
        subject: 'Ready to start cooking!',
        html: "<!DOCTYPE html>
<html>
<head>
</head>
<body>
<ul>
  <li>
    <a href='#{recipeLink}'>#{plateName}</a>
  </li>
  <li>
    <a href='#{recipeLink}'>#{recipeLink}</a>
  </li>
</ul>
</body>
</html>"
      }

      mg_client.send_message 'whatshouldimakefordinner.today', message_params

      puts mg_client
    end
  end
end
