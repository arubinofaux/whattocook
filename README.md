## README

Services required to run this project:

Heroku is used to host the app, and mailgun is used to process the email:

* [Heroku](https://www.heroku.com/)
* [Mailgun](http://www.mailgun.com/)

___

Deploying to Heroku is pretty straightforward:

    git clone git@gitlab.com:arubinofaux/whattocook.git
    cd whattocook
    heroku create
    git push heroku master

___

Get an active API key from mailgun:

[https://mailgun.com/app/account/security](https://mailgun.com/app/account/security)

___

Setup enviroment variables in Heroku:

    heroku config:set MAILGUN_SECRET=PLACE_MAILGUN_API_KEY

___

Setup scheduler on Heroku:

    heroku addons:create scheduler:standard
    heroku addons:open scheduler

___

Setup a Heroku Scheduler to run daily at a preferred time to execute:
`heroku addons:open scheduler` will open a website where you can set the scheduler configuration, use the image below as reference.

    rake get:recipe

![Heroku Scheduler](https://gitlab.com/arubinofaux/whattocook/uploads/ddf210145ec254adc9559aad88366ff9/Screen_Shot_2016-12-06_at_4.46.12_PM.png "Heroku Scheduler")

___

To update email address: open file `lib/tasks/get.rake` edit line 12: and run fallowing commands to update heroku with the new changes

    git add .
    git commit -m 'update email'
    git push heroku master

___

![Emails](https://gitlab.com/arubinofaux/whattocook/uploads/5c885c6daf5c8fe556e57d1e062ab237/Screen_Shot_2016-12-06_at_5.03.16_PM.png "Emails")
